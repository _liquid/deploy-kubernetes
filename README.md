# deploy-kubernetes

This is used to Deploy Kubernetes HA cluster on AWS

## Overview
In this project we will be using [Kops](https://github.com/kubernetes/kops/) to setup Kubernetes cluster on AWS. We will be setting up Highly Available (HA) Kubernetes Nodes.
Note: Assuming the deployment specifically to AWS us-east-1 region.

### Prerequisites
[AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) - Download and Install AWS-CLI
[Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) - Download and Install Kubectl
[Kops](https://github.com/kubernetes/kops#installing) - Download and Install Kops

### Setting Up IAM User for Kops
Create a new [IAM user](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) or use an existing IAM user and grant following permissions:
```
AmazonEC2FullAccess
AmazonRoute53FullAccess
AmazonS3FullAccess
AmazonVPCFullAccess
```

### Configuring AWS Cli by providing Access Key, Secret Access Key and AWS Region where Kubernetes cluster needs to be Installed

Execute below commands for setting up AWS CLI:
```
aws configure
AWS Access Key ID [None]: AccessKeyValue
AWS Secret Access Key [None]: SecretAccessKeyValue
Default region name [None]: us-east-1
Default output format [None]: json
```

### Creating AWS S3 bucket for storing Kops State
```
bucket_name=<valid-bucket-name>

aws s3api create-bucket \
--bucket ${bucket_name} \
--region us-east-1

aws s3api put-bucket-versioning --bucket ${bucket_name} --versioning-configuration Status=Enabled
```

### Setting up Kubernetes Cluster name and S3 Bucket URL
```
export KOPS_CLUSTER_NAME=example.k8s.local
export KOPS_STATE_STORE=s3://${bucket_name}
```

### Creating Kubernetes Cluster defination
The Node count and Node size depends on the workload which is planned to run Kubernetes cluster
```
kops create cluster \
--node-count=2 \
--node-size=t2.medium \
--zones=us-east-1a,us-east-1b \
--name=${KOPS_CLUSTER_NAME}
```

### Reviewing and Creating Kubernetes Cluster
```
kops edit cluster --name ${KOPS_CLUSTER_NAME}
kops update cluster --name ${KOPS_CLUSTER_NAME} --yes
```
Above command may take some time to create resources on AWS

### Setting up Kubernetes Dashboard
Execute below command to find kubernetes master information
```
kubectl cluster-info
```
Once cluster is up and running execute below commands to setup dashboard:
```
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```
To access Dashboard execute below command:
```
kubectl proxy
```
Enter following command to get *admin* user password
```
kops get secrets kube --type secret -oplaintext
```

![Dashboard Login](./images/admin-login.png)
Execute following command to find admin service accoount token:
```
kops get secrets admin --type secret -oplaintext
```
A sample service request page
![Service Request](./images/request.png)




